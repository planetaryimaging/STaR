## Description
Satellite Track and Read (STaR) is a general satellite scheduling and tracking program with modular motion control and signal processing for polar orbiting direct broadcasts. STaR runs on Gnu/Linux platforms.

## Website
Go to http://planetaryimaging.org to see recent pass images.

## License
STaR is released under the GNU General Public License GPLV3 as published by the Free Software Foundation. Third party components carry their own licenses. See https://www.gnu.org/licenses/gpl-3.0-standalone.html

## Installation and Documentation
Go to http://planetaryimaging.org/wiki/index.php?title=STaR for instructions on how to install and run.



