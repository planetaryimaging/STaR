#!/bin/bash

#  Copyright (C) 2020 Scott L. Williams.
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# script for AAPP processing (newly arrived)
# NOAA AVHRR hrpt data files.

# check number of args
NUMARGS=$#
if [ ! $NUMARGS -eq 2 ]
then
    echo "usage: aapp_process datapath sattime"
    exit 1
fi

# get path and sat/time stamp from args
DATAPATH=$1
SATTIME=$2

AAPP_PREFIX=/home/pi/AAPP_8.5/install/AAPP_8.5
export CSPP_IAPP_HOME=/usr/local/CSPP_IAPP_1_0
POLAR2GRID_HOME=/usr/local/polar2grid

# check for .hrpt file
if [ ! -f $DATAPATH/$SATTIME.hrpt ]
then 
  echo "aapp_process: data file $DATAPATH/$SATTIME.hrpt not found."
  exit 1
fi

# intermediate processing dir
PROC_DIR=/data/pi/SCRB/aapp

#############################################################################

# setup AAPP env variables
source $AAPP_PREFIX/ATOVS_ENV8

# make level1b directory for this pass
# create unique processing directory
export WRK=$PROC_DIR/$SATTIME/level1
rm -rf $WRK
mkdir -p $WRK
cd $WRK

echo "Starting $SATTIME.log" > $SATTIME.log
echo " " >> $SATTIME.log

echo "Running AAPP_RUN_NOAA" >> $SATTIME.log
AAPP_RUN_NOAA $DATAPATH/$SATTIME.hrpt  &>> $SATTIME.log

# check exit status
if [ $? -gt 0 ]
then
    echo "AAPP_RUN_NOAA failed." >> $SATTIME.log
    echo "exiting.." >> $SATTIME.log
    exit 1
fi
echo "AAPP succeeded." >> $SATTIME.log

echo "converting AAPP l1b to NOAA l1b." >> $SATTIME.log
avhrr_aapp_to_class hrpt_*.l1b $SATTIME.l1b &>> $SATTIME.log

echo "rendering preliminary images." >> $SATTIME.log
mkdir ../images
cd ../images

# use metopizer
/usr/local/bin/avhrr_to_ccsds --in ../level1/$SATTIME.l1b --image avhrr --bps 16 --jpeg &>> ../$SATTIME.log

###############################################################################

# sounders:

# High Resolution Infrared Radiation Sounder (HIRS),
# Advanced Microwave Sounding Unit-A (AMSU-A), 
# Microwave Humidity Sounder (MHS)

# setup IAPP env variables
source $CSPP_IAPP_HOME/cspp_iapp_env.sh      

# VERIFY: that AMSU-A, MHS is in AAPP produced hirsl1d file
/bin/mkdir -p $PROC_DIR/$SATTIME/sounders
cd $PROC_DIR/$SATTIME/sounders       

# parse which satellite
SAT=`echo $SATTIME | cut -c 1-7 | sed 's/-//' `
#echo $SAT

echo 'extracting sounder data'

$CSPP_IAPP_HOME/scripts/iapp_level2.sh --print_retrieval ../level1/hirsl1d_*.l1d $SAT

###############################################################################

# setup polar2grid environments
source $POLAR2GRID_HOME/bin/polar2grid_env.sh

echo 'projecting raw avhrr to SCRB land'
/bin/mkdir -p $PROC_DIR/$SATTIME/avhrr
cd $PROC_DIR/$SATTIME/avhrr

$POLAR2GRID_HOME/bin/polar2grid.sh avhrr gtiff -g lcc_fit -f ../level1/hrpt_*.l1b
    
echo 'creating hdf file'

$POLAR2GRID_HOME/bin/polar2grid.sh avhrr hdf5 \
				       --add-geolocation \
	-f ../level1/hrpt_*.l1b  > avhrr2hdf5.out 2> avhrr2hdf5.err

##############################################################################

# move everything to $DATAPATH
cd ../..
rm -rf $DATAPATH/$SATTIME
mv $SATTIME $DATAPATH

# end 
