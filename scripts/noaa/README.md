## Description
STaR's NOAA 18,19 HRPT decoder based on GnuRadio gr-noaa module.

## Website
Go to http://planetaryimaging.org to see recent NOAA 18,19 images.

## License
STaR is released under the GNU General Public License GPLV3 as published by the Free Software Foundation. Third party components carry their own licenses.

## Installation and Documentation
This is part of Planetary Imaging's STaR package. Go to http://planetaryimaging.org/wiki/index.php?title=STaR for instructions on how to integrate this project into an existing STaR installation. Otherwise, the decoder and example scripts for processing can be downloaded from https://gitlab.com/planterayimaging/star/scripts/noaa


